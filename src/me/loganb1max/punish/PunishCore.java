/*
 * Created By Logan Barrett / Loganb1max
 * 3/9/2017
 */

package me.loganb1max.punish;

import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.loganb1max.punish.commands.PunishCommand;

public class PunishCore extends JavaPlugin{

	private static PunishCore instance;
	
	public void onEnable() {
		instance = this;
		instance.getCommand("punish").setExecutor(new PunishCommand());
		instance.getLogger().log(Level.INFO, "Punish Enabled");
	}
	
	public static PunishCore getInstance() { return instance; }
	public static void registerListener(Listener object) {
		Bukkit.getPluginManager().registerEvents(object, getInstance());
		getInstance().getLogger().log(Level.INFO, "LISTENER: " + object.getClass().getName() + " HAS BEEN REGISTERED");
	}
}
