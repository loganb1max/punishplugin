/*
 * Created By Logan Barrett / Loganb1max
 * 3/9/2017
 */

package me.loganb1max.punish.gui;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import me.loganb1max.punish.PunishCore;

public class PunishGUI implements Listener{

	private Player target, moderator;
	private Inventory gui1, gui2;
	private ItemStack banItem, cheatingItem, hackItem;
	
	public PunishGUI(Player target, Player moderator) {
		PunishCore.registerListener(this);
		this.target = target;
		this.moderator = moderator;
		loadGUI();
		moderator.openInventory(gui1);
	}
	
	private void loadGUI() {
		banItem = new ItemStack(Material.BARRIER);
		ItemMeta banItemMeta = banItem.getItemMeta();
		banItemMeta.setDisplayName(ChatColor.RED + "Ban Player");
		banItem.setItemMeta(banItemMeta);
		gui1 = Bukkit.createInventory(moderator, 9, "Punishment Menu");
		gui1.setItem(4, banItem);
		cheatingItem = new ItemStack(Material.BOOK);
		ItemMeta cheatingItemMeta = cheatingItem.getItemMeta();
		cheatingItemMeta.setDisplayName(ChatColor.YELLOW + "Admitted to Cheating");
		ArrayList<String> cheatingLore = new ArrayList<String>();
		cheatingLore.add(ChatColor.GRAY + "14 day ban for admitting to cheating.");
		cheatingItemMeta.setLore(cheatingLore);
		cheatingItem.setItemMeta(cheatingItemMeta);
		hackItem = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta hackItemMeta = hackItem.getItemMeta();
		hackItemMeta.setDisplayName(ChatColor.DARK_RED + "Hacked Client");
		ArrayList<String> hackLore = new ArrayList<String>();
		hackLore.add(ChatColor.GRAY + "Perm ban for Hacked Client.");
		hackItemMeta.setLore(hackLore);
		hackItem.setItemMeta(hackItemMeta);
		gui2 = Bukkit.createInventory(moderator, 9, "Punishment Choice Menu");
		gui2.setItem(2, cheatingItem);
		gui2.setItem(6, hackItem);
	}
		
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if (e.getClickedInventory().equals(gui1) || e.getClickedInventory().equals(gui2)) {
			if (e.getSlotType().equals(SlotType.OUTSIDE)) return;
			if (e.getCurrentItem().equals(null) || e.getCurrentItem().getType().equals(Material.AIR)) return;
			if (e.getClickedInventory().equals(gui1)) {
				if (e.getCurrentItem().equals(banItem))
				{
					moderator.openInventory(gui2);
				}
			}
			else if (e.getClickedInventory().equals(gui2)) {
				if (e.getCurrentItem().equals(cheatingItem))
				{
					moderator.performCommand("tempban " + target.getDisplayName() + " 14d \"Admitted To Cheating\"");
					moderator.closeInventory();
				} else if (e.getCurrentItem().equals(hackItem)) {
					moderator.performCommand("ban " + target.getDisplayName() + " \"Hacked Client\"");
					moderator.closeInventory();
				}
			}
			e.setCancelled(true);
		} else return;
	}
}
