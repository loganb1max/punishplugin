/*
 * Created By Logan Barrett / Loganb1max
 * 3/9/2017
 */

package me.loganb1max.punish.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import me.loganb1max.punish.gui.PunishGUI;

public class PunishCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			if (sender.hasPermission("punishgui.staff")) {
				if (args.length == 1) {
					Player target = Bukkit.getPlayer(args[0]);
					if (target != null) {
						new PunishGUI(target, (Player)sender);
						return true;
					}
				}
			}
		}
		return false;
	}

}
